CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Allowed Formats Extension Widget module enables a new widget option for text
fields. This allows site builders to restrict the allowed formats by form 
display(after restricting by field definition, via the Allowed Formats module).
This module was inspired by this issue: [Store allowed format settings with 
widget ](https://www.drupal.org/node/2884070)
And the patch contributed by [lisastreeter](https://www.drupal.org/u/lisastreeter)

This functionality is useful when a text field must be reused within the same
entity type with different formats each time. Our case use was this:

We had a set of paragraphs that represented different components, all of them 
had a part of text. Sometimes, that text was formatted, sometimes it allowed
lists, or embedded images. We created the different text formats and a paragraph
with a text field.
We enabled different displays for this paragraph, one for each case. Then,
in the content type using the paragraphs, we added a field for each type,
and specified the display to use.


 * For a full description of the module visit:
   https://www.drupal.org/project/allowed_formats_widget_extension

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/allowed_formats_widget_extension


REQUIREMENTS
------------

This module requires the [Allowed Formats](https://www.drupal.org/project/allowed_formats)  module to work.


INSTALLATION
------------

 - Install the Allowed Formats Extension Widget module as you would normally 
   install a contributed Drupal module. 
   Visit https://www.drupal.org/node/1897420 
   for further information.


CONFIGURATION
-------------

- Enable Allowed formats for a specific field: 
  ![Enable all the desired text formats in the field definition]()
- In the form display tab, select the Allowed formats
  Extension Widget for that same field:
  ![Change the widget of the field in the Form Display tab of your entity.]()
  ![ Pick Allowed Formats Widget Extension]()
- In the widget settings, pick the allowed formats for this field,
  in this display only: 
  ![In the widget options appear new checkboxes]()
  ![one for each allowed text format.]()
  ![There the site builder can pick which one to enable for this display]()
- This way, the same field can be reused with different 
  settings in different displays.


MAINTAINERS
-----------

Current maintainers:

 - Ana Colautti (anacolautti) - https://www.drupal.org/u/anacolautti
 - C�sar Miquel (cesarmiquel) - https://www.drupal.org/u/cesarmiquel
 - Lucas Mingarro (lmingarro) - https://www.drupal.org/u/lmingarro
