<?php

namespace Drupal\afew\Plugin\Field\FieldWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\text\Plugin\Field\FieldWidget\TextareaWidget;

/**
 * A widget bar.
 *
 * @FieldWidget(
 *   id = "afwe",
 *   label = @Translation("Allowed Formats Widget Extension"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary"
 *   }
 * )
 */
class TextFormatWidget extends TextareaWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'allowed_formats' => [],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $options = [];

    $settings = $this->fieldDefinition->getThirdPartySettings('allowed_formats');

    foreach (filter_formats() as $format) {
      if (in_array($format->id(), $settings['allowed_formats'])) {
        $options[$format->id()] = $format->label();
      }
    }
    $element['allowed_formats'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed formats'),
      '#options' => $options,
      '#default_value' => $this->getSetting('allowed_formats'),
      '#description' => $this->t('Restrict which text formats are allowed, given the user has the required permissions, and the format has been set in the field definition. Text formats must be enabled at the field definitions.'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $options = [];
    foreach (filter_formats() as $format) {
      $options[$format->id()] = $format->label();
    }

    $allowed_formats = $this->getSetting('allowed_formats');
    if (is_array($allowed_formats)) {
      $allowed_formats = array_filter($allowed_formats);
      if (!empty($allowed_formats)) {
        $labels = [];
        foreach ($allowed_formats as $format) {
          $labels[] = $options[$format];
        }
        $summary[] = $this->t('Allowed formats: @allowed_formats', [
          '@allowed_formats' => implode(", ", $labels),
        ]);
      }
    }

    return $summary;
  }

}
